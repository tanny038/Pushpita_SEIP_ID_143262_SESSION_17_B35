<?php

class Myclass{

    public $prop1 = 'Its class property';
    public $prop2 = 'We are students';

    public function setProp2($setValue){
        $this -> prop2 = $setValue;
    }

    public function getProp2(){
        return $this -> prop2 . "<br/>";
    }

    public function setProp1($setValue){
        $this -> prop1 = $setValue;
    }

    public function getProp1(){
        return $this -> prop1 . "<br/>";
    }


}

$obj1 = new Myclass;
$obj2 = new Myclass;
/*var_dump($obj1)."<br/>";*/
/*echo $obj2-> prop2 . "<br/>";*/

echo $obj1-> getProp1() . "<br/>";
$obj1 -> setProp1("Its new one");
echo $obj1-> getProp1() . "<br/>";

echo $obj2-> getProp2() . "<br/>";
$obj2 -> setProp2("We are new");
echo $obj2-> getProp2() . "<br/>";

/*var_dump($obj2) . "<br/>";*/

/*echo $obj2-> prop2 . "<br/>";*/


?>

<br/><br/>



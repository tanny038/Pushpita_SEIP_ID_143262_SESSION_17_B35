<?php

class Student{

    public $student_ID;
    public $student_Name;
    public $cgpa;

    public function setStudentID($setID){
        $this -> student_ID = $setID;
    }

    public function getStudentID(){
        return $this -> student_ID . "<br/>";
    }

    public function setStudentName($setName){
        $this -> student_Name = $setName;
    }

    public function getStudentName(){
        return $this -> student_Name . "<br/>";
    }

    public function setCgpa($setcgpa){
        $this -> cgpa = $setcgpa;
    }

    public function getCgpa(){
        return $this -> cgpa . "<br/>";
    }


}

$obj1 = new Student;

echo $obj1-> getStudentID() . "<br/>";
$obj1 -> setStudentID(12205001);
echo $obj1-> getStudentID() . "<br/>";

echo $obj1-> getStudentName() . "<br/>";
$obj1 -> setStudentName("Gobinda Chakrabarty");
echo $obj1-> getStudentName() . "<br/>";

echo $obj1-> getCgpa() . "<br/>";
$obj1 -> setCgpa(3.58);
echo $obj1-> getCgpa() . "<br/>";

?>

<br/><br/>